//
//  DetailViewController.swift
//  testRaul
//
//  Created by Aleix Cos on 20/5/22.
//

import UIKit
import Kingfisher
import Alamofire

class DetailViewController: UIViewController {
    
    private let data: [Profile] = [
        .init(imageURL: URL(string: "https://picsum.photos/200/300")!, text: "Raul"),
        .init(imageURL: URL(string: "https://picsum.photos/200/300")!, text: "Aleix")
    ]

    private lazy var collectionView : UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.minimumLineSpacing = 10
        collectionViewLayout.minimumInteritemSpacing = 10
        collectionViewLayout.scrollDirection = .vertical
        collectionViewLayout.estimatedItemSize = CGSize(width: 50, height: 50)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.register(DetailCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: DetailCollectionViewCell.self))
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = self
        return collectionView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addViews()
        collectionView.reloadData()
    }
    
}

private extension DetailViewController {
    func addViews() {
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension DetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { data.count }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DetailCollectionViewCell.self), for: indexPath) as? DetailCollectionViewCell
        else { return UICollectionViewCell() }
        let profile = data[indexPath.item]
        cell.updateUI(imageURL: profile.imageURL, text: profile.text)
        return cell
    }
    
}

struct Profile {
    let imageURL: URL
    let text: String
}
