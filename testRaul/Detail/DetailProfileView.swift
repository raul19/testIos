//
//  DetailProfileView.swift
//  testRaul
//
//  Created by Raul Moreno on 5/8/22.
//

import Foundation
import UIKit
import Kingfisher

class DetailProfileView : UIView {
    
    private let imageView = UIImageView()
    private let titleLabel: UILabel = {
       let titleLabel = UILabel()
        titleLabel.textColor = .green
        return titleLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateUI(imageURL: URL, text: String) {
        imageView.kf.setImage(with: imageURL)
        titleLabel.text = text
    }
}

private extension DetailProfileView {
    func addViews() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 50),
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
