//
//  DetailCollectionViewCell.swift
//  testRaul
//
//  Created by Raul Moreno on 5/8/22.
//

import Foundation
import UIKit

class DetailCollectionViewCell : UICollectionViewCell {
    
    private let profileView = DetailProfileView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateUI(imageURL: URL, text: String) {
        profileView.updateUI(imageURL: imageURL, text: text)
    }
    
}


private extension DetailCollectionViewCell {
    func addViews() {
        profileView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(profileView)
        NSLayoutConstraint.activate([
            profileView.topAnchor.constraint(equalTo: topAnchor),
            profileView.leadingAnchor.constraint(equalTo: leadingAnchor),
            profileView.trailingAnchor.constraint(equalTo: trailingAnchor),
            profileView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
