//
//  ViewController.swift
//  testRaul
//
//  Created by Aleix Cos on 6/5/22.
//

import UIKit

class ViewController: UIViewController {

    private let data = ["view 1",
                        "view 2",
                        "view 3",
                        "view 4",
                        "view 5"]

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }



}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { data.count }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "labelCell", for: indexPath)
        let dataText = data[indexPath.item]
        if let label = cell.viewWithTag(1) as? UILabel {
            label.text = dataText

        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = DetailViewController()
        navigationController?.pushViewController(detail, animated: true)
    }

    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        print("higlighted: \(indexPath.row)")
        let cell = tableView.cellForRow(at: indexPath)
        cell?.selectionStyle = .default
    }

    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        print("unhiglighted: \(indexPath.row)")
        let cell = tableView.cellForRow(at: indexPath)

        cell?.selectionStyle = .none
    }
}

